/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		/* TODO: write this. */
		this->root = 0;
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		/* TODO: write this. */
		return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		return false;  //just so it compiles.  you of course need to do something different.
	}
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)
	{
		listNode* p;
		p = L;
		while (L != NULL)
		{
			p=p->next;
		}
		L=x;
	}

	void list::remove(val_type x)
	{
		/* TODO: write this. */
		listNode* p;
		p = root;
		p->data = x;
		p->next = 
		while(p != NULL){
			if(p->data != x)
				p = p->next;
			else{
				delete p;
				p = p->next;
				break;
			}	
				
		}
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p) 
			return true;
		else 
			return false;
	}

	unsigned long list::length() const
	{
		/* TODO: write this. */
		listNode* p;
		p = root;
		int counter = 0;
		while(p != NULL){
			counter++;
			p = p->next;
		}
			
		return counter;	
		
		
		
		
		//return 0; //just so it compiles.  you of course need to do something different.
	}

	void list::merge(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2)
	{
		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
		listNode* p;
		listNode* q;
		int length1 = 0;
		int length2 = 0;
		p = L1;
		q = L2;
		if(p==NULL||q==NULL)
			{return null;}
		while (p != NULL)
		{
			length1++;
			p = p->next;
		}
		while (q != NULL)
		{
			length2++;
			q = q->next;
		}

		//length1 = length(L1);
		//length2 = length(L2); (faster, but relies on another part of the code)

		int i = 0;
		while(i < length1)
		{
			p = L1;
			while(p!=NULL)
			{
				int n = 0;
				while (n < length2)
				{
					q = L2;
					while(q!=NULL)
					{
						if (q==p)
						{
							*this = p;
							this = this->next;
						}
						q = q->next;
						n++;
					}
				}
				p = p->next;
				i++;
			}
		}
		int L3 = length(this->data);
		for (int w = 0; w < L3-1; w++)
		{
			cout << this->data;
			(this->data).remove;
		}
	}
}
